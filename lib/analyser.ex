defmodule Analyser do
  @moduledoc """
  Documentation for Analyser.

  TODOS
   - fn application
      - more than 1 arg
   - send
   - (more than 1) match arms
   - modules (defmodule, def, etc)
   - differentiate vars by more than name (multiple x possible)

  """
  @type label :: [line: integer(), column: integer()]
  @type ast :: {atom(), label(), any()}

  @type abstractRepresentation :: {:r, atom()}
  @type abstractCache :: {:C, label()}
  @type abstractConstant :: {:const, any()}
  @type abstractSpawn :: {:spawn, label()}
  @type abstract ::
          abstractCache() | abstractConstant() | abstractRepresentation() | abstractSpawn()
  @type placerholder :: {:apply, label(), any(), any()} | {:send, any(), any(), label()}
  @type constrain :: list({:<=, abstract | placerholder, abstract | placerholder})

  @spec ast(String.t()) :: {:ok, ast()} | {:error, any()}
  def ast(filename) do
    Code.string_to_quoted(File.read!(filename),
      file: filename,
      columns: true,
      token_metadata: false
    )
  end

  def get_receives({:receive, l, body}) do
    [Constraints.label(l)] ++ get_receives(body)
  end

  def get_receives([]) do
    []
  end

  def get_receives(stmnts) when is_list(stmnts) do
    Enum.flat_map(stmnts, &get_receives/1)
  end

  def get_receives({_, body}) do
    get_receives(body)
  end

  def get_receives({_, _, body}) do
    get_receives(body)
  end

  def get_receives(i) when is_nil(i) or is_atom(i) or is_number(i) do
    []
  end

  # second pass to expand application & send constrains
  @spec expand([constrain()]) :: [constrain()]
  def expand(cntrs) do
    exp = fn constrs ->
      {filtered_constrs, applies, sends} =
        Enum.reduce(
          constrs,
          {[], [], []},
          fn c, {cs, aps, ss} ->
            case c do
              {:apply, label, func, arg} -> {cs, aps ++ [{label, func, arg}], ss}
              {:send, recipient, message, label} -> {cs, aps, ss ++ [{recipient, message, label}]}
              v -> {cs ++ [v], aps, ss}
            end
          end
        )

      expand_apply = fn {l, func, arg} ->
        # (t_1)^(l_1) in book
        func_label = Constraints.label(func)

        List.flatten(
          dfs(filtered_constrs, {:C, func_label}, fn c ->
            case c do
              {:fn, param, body} ->
                body_cs =
                  case Constraints.label(body) do
                    {:const, v} -> {:const, v}
                    l -> Constraints.c(l)
                  end

                arg_cs =
                  case Constraints.label(arg) do
                    {:const, v} -> {:const, v}
                    l -> Constraints.c(l)
                  end

                [Constraints.subseteq(arg_cs, Constraints.r(param))] ++
                  [Constraints.subseteq(body_cs, Constraints.c(l))]

              _ ->
                []
            end
          end)
        )
      end

      expand_send = fn {r, message, _} ->
        receiver_label = Constraints.label(r)
        # first extract worker fns
        # this means we can't detect receives in main script body
        spawn_labels =
          Enum.filter(
            dfs(filtered_constrs, {:C, receiver_label}, fn c ->
              case c do
                {:spawn, label} -> label
                _ -> nil
              end
            end),
            fn v -> !is_nil(v) end
          )

        spawned_fns =
          Enum.flat_map(
            spawn_labels,
            fn l ->
              Enum.filter(
                dfs(
                  filtered_constrs,
                  {:spawn, l},
                  fn c ->
                    case c do
                      {:fn, _, body} ->
                        case body do
                          {:__block__, _, inner} ->
                            Enum.map(
                              Enum.filter(
                                inner,
                                fn s ->
                                  case s do
                                    {:receive, _, _} -> true
                                    _ -> false
                                  end
                                end
                              ),
                              fn {:receive, l, _} -> l end
                            )

                          _ ->
                            [Constraints.label(body)]
                        end

                      _ ->
                        nil
                    end
                  end
                ),
                fn v -> !is_nil(v) end
              )
            end
          )

        spawned_fns = Enum.flat_map(spawned_fns, &Function.identity/1)

        # 2. find all rcvs in these worker fns
        receives =
          Enum.flat_map(
            spawned_fns,
            fn body_label ->
              Enum.filter(
                dfs(filtered_constrs, {:C, body_label}, fn c ->
                  case c do
                    {:rcv, l} -> l
                    _ -> nil
                  end
                end),
                fn v -> !is_nil(v) end
              )
            end
          )

        # 3. find all receiving do: blocks
        full =
          Enum.flat_map(
            receives,
            fn l ->
              dfs(filtered_constrs, {:C, l}, fn {:fn, param, _} ->
                m =
                  case message do
                    {_, message_label, _} -> Constraints.c(message_label)
                    v -> {:const, v}
                  end

                [Constraints.subseteq(m, Constraints.r(param))]
              end)
            end
          )

        List.flatten(full)
      end

      expanded_sends = Enum.flat_map(sends, expand_send)
      expanded_applies = Enum.flat_map(applies, expand_apply)
      filtered_constrs ++ expanded_applies ++ expanded_sends
    end

    Enum.reduce_while(
      Stream.cycle([nil]),
      cntrs,
      fn _, prev ->
        case exp.(prev) do
          ^prev -> {:halt, prev}
          expanded -> {:cont, expanded}
        end
      end
    )
  end

  @spec get_fns([constrain()], label()) :: [constrain()]
  def get_fns(constrs, label) do
    Enum.uniq(
      Enum.flat_map(
        dfs(constrs, {:C, label}, &Function.identity/1),
        fn e ->
          case e do
            {:fn, _, _} -> [e]
            {:const, v} -> [v]
            _ -> []
          end
        end
      )
    )
  end

  def get_valid_receives(constrs, label) do
    spawn_labels =
      Enum.flat_map(
        dfs(constrs, {:C, label}, &Function.identity/1),
        fn e ->
          case e do
            {:spawn, l} -> [l]
            _ -> []
          end
        end
      )

    r =
      Enum.concat(
        Enum.flat_map(
          spawn_labels,
          fn l ->
            dfs(constrs, {:spawn, l}, fn e ->
              case e do
                {:fn, _, _} -> get_receives(e)
                {:const, v} -> [v]
                _ -> []
              end
            end)
          end
        )
      )

    Enum.uniq(r)
    spawn_labels
  end

  # depth first search
  @spec dfs([constrain()], abstract(), (constrain() -> b)) :: [b] when b: var
  def dfs(constrs, from, operation) do
    children =
      Enum.flat_map(
        constrs,
        fn c ->
          case c do
            {:<=, a, ^from} -> [a]
            _ -> []
          end
        end
      )

    recurse = fn c -> (&dfs/3).(constrs, c, operation) end
    Enum.map(children, operation) ++ Enum.flat_map(children, recurse)
  end

  def get_sends({:send, l, [target, message]}) do
    [{Constraints.label(l), Constraints.label(target)}] ++ get_sends(target) ++ get_sends(message)
  end

  def get_sends({:do, stmnts}) do
    get_sends(stmnts)
  end

  def get_sends({_, [line: _, column: _], stmnts}) when is_list(stmnts) do
    get_sends(stmnts)
  end

  def get_sends({_, [line: _, column: _], stmnt}) do
    get_sends(stmnt)
  end

  def get_sends(stmnts) when is_list(stmnts) do
    Enum.flat_map(stmnts, &get_sends/1)
  end

  def get_sends(tuple) when is_tuple(tuple) do
    # to perform an all elements of tuples we have to convert to list first
    Enum.flat_map(
      Tuple.to_list(tuple),
      &get_sends/1
    )
  end

  def get_sends(i) when is_nil(i) or is_atom(i) or is_number(i) do
    []
  end

  @spec solve(String.t(), label(), :fns | :rcvs | ([constrain()], label() -> [constrain()])) :: [
          constrain()
        ]
  def solve(file, label, :fns) do
    solve(file, label, &get_fns/2)
  end

  def solve(file, label, :rcvs) do
    solve(file, label, &get_valid_receives/2)
  end

  def solve(file, label, extractor) do
    {:ok, extracted_ast} = ast(file)
    constrs = expand(Constraints.constrains(extracted_ast))
    extractor.(constrs, label)
  end
end
