defmodule Constraints do
  @doc """
  Hello world.

  ## Examples

      iex> Analyser.constrains([])
      []

  """

  @type label :: [line: integer(), column: integer()]
  @type ast :: {atom(), label(), any()}

  @type abstractRepresentation :: {:r, atom()}
  @type abstractCache :: {:C, label()}
  @type abstractConstant :: {:const, any()}
  @type abstractSpawn :: {:spawn, label()}
  @type abstract ::
          abstractCache() | abstractConstant() | abstractRepresentation() | abstractSpawn()
  @type placerholder :: {:apply, label(), any(), any()} | {:send, any(), any(), label()}
  @type constrain :: list({:<=, abstract | placerholder, abstract | placerholder})

  @spec c(l) :: {:C, l} when l: label()
  def c(x) do
    {:C, x}
  end

  @spec r(variable) :: {:r, variable} when variable: atom
  def r(x) do
    {:r, x}
  end

  @spec subseteq(a, b) :: {:<=, a, b} when a: var, b: var
  def subseteq(a, b) do
    {:<=, a, b}
  end

  @spec label(any()) :: label() | abstractConstant()
  def label([{:line, _}, {:column, _}] = l) do
    l
  end

  def label([{:closing, _} | l]) do
    label(l)
  end

  def label([{:newlines, _} | l]) do
    label(l)
  end

  def label([{:do, _}, {:end, _} | l]) do
    label(l)
  end

  def label({{:., _, [{_, l, _}]}, _, [_]}) do
    l
  end

  def label({_, l, _}) do
    l
  end

  def label(v) when is_atom(v) or is_number(v) do
    {:const, v}
  end

  @spec assignments({:=, label(), [ast()]}) :: {atom(), ast()}
  defp assignments({:=, _, [{target, _, _}, value]}) do
    {target, value}
  end

  # blocks
  @spec constrains(ast) :: [constrain()]
  def constrains({:__block__, _, lines}) do
    # no support for empty lines in __block__ yet, we just assume every item is on the next line by using index
    reducer = fn line ->
      add_constrains =
        case line do
          {:=, _, _} ->
            {target, value} = assignments(line)

            value_constrain =
              case label(value) do
                {:const, v} -> {:const, v}
                l -> c(l)
              end

            # this is really tricky
            # C(value) is easy, C(l1) <= r(x) seems ok
            # but because of implicit scope we don't have a good label
            # for the assignment itself vs the scope it's valid in
            # {C(l2) <= C(l)} page 174 [let] definition
            # maybe it's okay

            constrains(value) ++
              [subseteq(value_constrain, r(target))]

          _ ->
            constrains(line)
        end

      add_constrains
    end

    # this might not be enough as it does not use a label for this block/scope.
    Enum.flat_map(
      lines,
      reducer
    )
  end

  # spawn
  def constrains({:spawn, _, [worker]} = e) do
    l = label(e)
    worker_label = label(worker)

    worker_constrains = constrains(worker)

    sends =
      Enum.filter(
        Enum.map(
          worker_constrains,
          fn v ->
            case v do
              {:send, _, _, s_l} -> subseteq({:send, s_l}, {:spawn, l})
              _ -> nil
            end
          end
        ),
        fn c -> !is_nil(c) end
      )

    [
      subseteq(c(worker_label), {:spawn, l}),
      subseteq({:spawn, l}, c(l))
    ] ++ worker_constrains ++ sends
  end

  # receive
  def constrains({:receive, _, [[do: [receiver]]]} = e) do
    l = label(e)
    # currently only support a single match!
    receiver_label = label(receiver)
    # build something that looks like a fn def to reuse constrains for functions
    fake_fn = {:fn, receiver_label, [receiver]}
    [subseteq({:rcv, receiver_label}, c(l))] ++ constrains(fake_fn)
  end

  # send
  def constrains({:send, _, [recipient, message]} = e) do
    l = label(e)
    # we have to unpack previous constrains here
    # for this I have no eval strategy yet
    [{:send, recipient, message, l}] ++ constrains(recipient) ++ constrains(message)
  end

  def constrains({:fn, _, [{:->, _, [vars, body]}]} = e) do
    l = label(e)
    # no multiple match arms supported yet
    # only a single variable can be used
    var =
      case vars do
        [] -> nil
        [{var, _, _}] -> var
      end

    # = { {fn x => e0} <= C(l) }
    [subseteq({:fn, var, body}, c(l))] ++ constrains(body)
  end

  # assignment
  def constrains({:=, _, rest} = e) do
    l = label(e)
    # this should only happen for assignments that are not part of a __block__
    # as the assigned target has no scope to be valid in it's not interesting
    # but (x = value) evaluates to value too so we should at the least treat it the same as we would a simple value statement
    # such assignments might also be used for special cases I don't know about
    {_, value} = assignments({:=, l, rest})
    constrains(value)
  end

  # application
  def constrains({{:., _, [func]}, _, [arg]} = v) do
    # only supports single argument application for now
    # so f.(x) works, but f.(x, y) not (yet)

    # use the label of the function itself
    # we don't want the dot in my_func.(1) to be the label but the beginning of func name my_func
    constrains(func) ++ constrains(arg) ++ [{:apply, label(v), func, arg}]
  end

  # variables
  def constrains({var, _, nil} = e) when is_atom(var) do
    l = label(e)
    [subseteq(r(var), c(l))]
  end

  # fallback with label
  def constrains({i, _, _} = e) do
    [subseteq({:const, i}, c(label(e)))]
  end

  # fallback
  def constrains(_) do
    # assume constant as fallback
    []
  end
end
