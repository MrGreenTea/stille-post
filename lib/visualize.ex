defmodule Visualize do
  import Analyser

  def upper_spawns(constrs, from) do
    filter = fn e ->
      case e do
        {:spawn, _} -> true
        _ -> false
      end
    end

    children =
      Enum.flat_map(
        constrs,
        fn c ->
          case c do
            {:<=, ^from, a} -> [a]
            _ -> []
          end
        end
      )

    recurse = fn c -> (&upper_spawns/2).(constrs, c) end

    r = Enum.filter(children, filter) ++ Enum.flat_map(children, recurse)

    case r do
      # we assume if no spawn can be found in the ast tree "above" this send it's from main process
      [] -> [:main_process]
      [s | _] -> [s]
    end
  end

  @spec associate(String.t(), [{any, any}]) :: map
  def associate(file, sends) do
    {:ok, extracted_ast} = ast(file)
    constrs = Constraints.constrains(extracted_ast)

    Enum.reduce(
      sends,
      %{},
      fn {send_label, target_label}, acc ->
        up_spawn = upper_spawns(constrs, {:send, send_label})

        sol =
          Enum.map(
            solve(file, target_label, :rcvs),
            fn s -> {send_label, s} end
          )

        Map.merge(
          acc,
          %{up_spawn => sol},
          fn _k, a, b ->
            a ++ b
          end
        )
      end
    )
  end

  def label_to_id(:main_process) do
    # A_ prefix so it's sorted to start in diagram source
    "A_MAINPROCESS[main process]"
  end

  def label_to_id(line: l, column: c) do
    "LINE_#{l}_COLUMN_#{c}[line: #{l}, column: #{c}]"
  end

  def label_to_id({:spawn, l}) do
    label_to_id(l)
  end

  @spec main([String.t()]) :: 0
  def main([file]) do
    {:ok, extracted_ast} = ast(file)
    sends = get_sends(extracted_ast)

    result = associate(file, sends)

    flattened =
      Enum.flat_map(
        result,
        fn {k, v} ->
          Enum.flat_map(k, fn i -> Enum.map(v, fn {s, u} -> {i, u, s} end) end)
        end
      )

    diagram_sends =
      "   " <>
        Enum.join(
          Enum.sort(
            Enum.map(
              flattened,
              fn {p, l, [line: sl, column: sc]} ->
                label_to_id(p) <> "-- line: #{sl}, column: #{sc} -->" <> label_to_id(l)
              end
            )
          ),
          "\n   "
        )

    diagram = "graph TD\n#{diagram_sends}"

    IO.puts(diagram)

    0
  end
end
