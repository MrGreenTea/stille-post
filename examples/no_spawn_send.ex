f = fn x ->
  receive do
    v -> v
  end
end

send(f, 1)
