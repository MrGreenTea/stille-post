p =
  spawn(fn ->
    receive do
      {:msg, value} -> value
    end
  end)

send(p, {:msg, 1})
send(p, {:no_msg, 2})
