target =
  spawn(fn ->
    receive do
      v -> IO.puts(v)
    end
  end)

sender =
  spawn(fn ->
    receive do
      pid -> send(pid, 0)
    end
  end)

send(sender, target)
:timer.sleep(1)
