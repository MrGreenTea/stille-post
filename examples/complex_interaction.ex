proc1 =
  spawn(fn ->
    receive do
      pid1 -> send(pid1, 1)
    end
  end)

proc2 =
  spawn(fn ->
    receive do
      pid2 -> send(pid2, 2)
    end
  end)

proc3 =
  spawn(fn ->
    send(proc2, proc3)
    send(proc1, proc3)

    receive do
      # ToDo
      pid3 -> send(pid3, 1)
    end
  end)

send(proc1, proc2)
send(proc2, proc1)
send(proc3, proc1)
