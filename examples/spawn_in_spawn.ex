p =
  spawn(fn ->
    inner =
      spawn(fn ->
        receive do
          v -> v
        end
      end)

    send(inner, 2)
  end)
