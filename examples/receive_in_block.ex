proc =
  spawn(fn ->
    a = 1

    receive do
      p -> send(p, a)
    end
  end)

send(proc, spawn(fn -> nil end))
