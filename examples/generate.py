PROCS = 511

with open("new_example.ex", "w") as f:
    for i in range(PROCS):
        f.write(f"pid{i} = spawn(fn -> receive do\n  p{i} -> send(pid{(i+1) % PROCS}, p{i}) end end)\n")
    f.write(f"send(pid{i}, pid{i-1})\n")
