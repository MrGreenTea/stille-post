out = spawn(
  fn ->
    receive do
      p ->
        v = spawn(fn -> nil end)
        send(p, v)
    end
  end
)
