send(
  spawn(fn ->
    receive do
      pid -> send(pid, 1)
    end
  end),
  spawn(fn ->
    receive do
      _ ->
        send(
          spawn(fn ->
            receive do
              pid2 -> send(pid2, 1)
            end
          end),
          spawn(fn ->
            receive do
              v -> v
            end
          end)
        )
    end
  end)
)
