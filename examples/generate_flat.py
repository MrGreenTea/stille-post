PROCS = 511

with open("flat_send.ex", "w") as f:
    for p in range(PROCS):
        f.write(f"p{p} = spawn(fn -> receive do: (v{p} -> v{p}) end)\n")
        f.write(f"send(p{p}, 0)\n")