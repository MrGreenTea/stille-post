import sys

PROCS = int(sys.argv[1])

with open("flat_send.ex", "w") as f:
    for p in range(PROCS):        
        f.write(f"p{p} = spawn(fn -> receive do: (v{p} -> v{p}) end)\n")
        f.write(f"send(p{p}, {p})\n")