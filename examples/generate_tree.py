DEPTH = 9

def proc(i):
    return f"p{i} = spawn(fn -> receive do: (v{i} -> send(p{i}1, v{i}); send(p{i}2, v{i})) end)"

def write(f, i):
    f.write(proc(i) + "\n")
    if len(i) < DEPTH - 1:
        write(f, i+"1")
        write(f, i+"2")

with open("process_tree.ex", "w") as f:
    write(f, "")