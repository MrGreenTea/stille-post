# Usage
Compile with ``mix escript.build``
Then try running with
``./analyser <file> <line> <column>``.
For example this:
``./analyser examples/basic.ex 4 12``


There are some code samples in [examples/](test/) to try out.

# IMPORTANT
the following are not (yet) supported:

- duplicate variable name. Each variable has to have a unique name globaly.
- code blocks only work partially. Variables are only "valid" until the next statement.
  This works:
  ```elixir
  x = 1
  y = x
  ```
  This does not
  ```elixir
  x = 1
  IO.inspect(x)
  y = x
  ```
- functions with more than 1 argument. `fn x -> x end` works, `fn x, y -> x + y end` does not.
- applying more than 1 argument to a function. `f.(1)` works, `f.(1, 2)` does not.
- `self()` is not treated as a valid target for `send()` yet.
- nested `receive`. This does not work well:
  ```elixir
  p =
  spawn(fn ->
    receive do
      a ->
        receive do
          b -> IO.inspect({a, b})
        end
    end
  end)

  send(p, 1)
  send(p, 2)
  ```