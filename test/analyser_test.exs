defmodule AnalyserTest do
  use ExUnit.Case
  import Analyser
  import Constraints
  import Visualize

  doctest Analyser

  test "constrains fn id" do
    {:ok, ast} = Analyser.ast("examples/fn.ex")

    assert [
             {:<=, {:fn, :x, {:x, l1, _}}, {:C, _}},
             {:<=, {:r, :x}, {:C, l1}}
           ] = constrains(ast)
  end

  test "assignment constrains" do
    {:ok, ast} = Analyser.ast("examples/assignments.ex")
    # empty list is currently the label of __block__

    constrains = constrains(ast)
    result = Analyser.solve("examples/assignments.ex", [line: 3, column: 1], :fns)

    assert [
             {:<=, {:const, 1}, {:r, :x}},
             {:<=, {:r, :x}, {:C, l3}},
             {:<=, {:C, l3}, {:r, :y}},
             {:<=, {:r, :y}, {:C, l5}}
           ] = constrains

    assert [1] = result
  end

  test "cGet simple" do
    l1 = [line: 2, column: 1]
    l2 = [line: 1, column: 1]
    constrains = [{:<=, {:C, l2}, {:C, l1}}]
    result = Analyser.dfs(constrains, {:C, l1}, &Function.identity/1)
    assert [{:C, l2}] == result
  end

  test "cGet simple chain" do
    l3 = [line: 3, column: 1]
    l2 = [line: 2, column: 1]
    l1 = [line: 1, column: 1]

    constrains = [
      subseteq(c(l2), c(l1)),
      subseteq(c(l3), c(l2))
    ]

    result = Analyser.dfs(constrains, {:C, l1}, &Function.identity/1)
    assert [{:C, l2}, {:C, l3}] == result
  end

  test "test simple application expansion" do
    {:ok, ast} = Analyser.ast("examples/fn_application.ex")
    constrains = Analyser.expand(constrains(ast))

    assert [
             {:<=, {:fn, :x, {:x, l2, _}}, {:C, l1}},
             {:<=, {:r, :x}, {:C, l2}},
             {:<=, {:C, l1}, {:r, :f}},
             {:<=, {:r, :f}, {:C, l4}},
             {:<=, {:r, :f}, {:C, l5}},
             {:<=, {:C, l5}, {:r, :x}},
             {:<=, {:C, l2}, {:C, l3}}
           ] = constrains
  end

  test "first send example" do
    result = solve("examples/basic_send.ex", [line: 4, column: 12], :fns)

    assert [1] = result
  end

  test "associate basic send" do
    result = associate("examples/basic_send.ex", [{[line: 8, column: 1], [line: 8, column: 6]}])
    assert %{[:main_process] => [{[line: 8, column: 1], [line: 2, column: 3]}]} == result
  end

  test "only_constrains" do
    {:ok, ast} = Analyser.ast("examples/send_pid.ex")
    constrains = Analyser.expand(constrains(ast))

    assert Enum.all?(
             constrains,
             fn c -> elem(c, 0) == :<= end
           )
  end

  test "application result" do
    result = solve("examples/fn_application_result.ex", [line: 3, column: 5], :fns)

    assert [
             {:fn, :v, {:v, _, nil}}
           ] = result
  end

  test "inline application" do
    result = solve("examples/inline_application.ex", [line: 2, column: 1], :fns)

    assert [_, :x] = result
  end

  test "chained application" do
    result = solve("examples/chained_application.ex", [line: 2, column: 1], :fns)

    assert [:x, _] = result
  end

  test "ignore fns without spawn" do
    result = solve("examples/no_spawn_send.ex", [line: 3, column: 10], :fns)

    assert [] = result
  end

  test "only associate send to spawns" do
    result =
      associate("examples/no_spawn_send.ex", [{[line: 7, column: 1], [line: 7, column: 6]}])

    assert %{[:main_process] => []} == result
  end

  @tag :skip
  test "send pid" do
    result = solve("examples/send_pid.ex", [line: 9, column: 12], :rcvs)

    assert [1] = result
  end

  test "send pid solved" do
    result = solve("examples/send_pid.ex", [line: 11, column: 19], :rcvs)

    assert [[line: 2, column: 3]] = result
  end

  test "send pid and back correct processes" do
    file = "examples/send_pid.ex"

    {:ok, extracted_ast} = ast(file)
    sends = get_sends(extracted_ast)

    result = associate(file, sends)

    assert %{
             [:main_process] => [{[line: 15, column: 1], [line: 9, column: 3]}],
             # currently includes sends from "main_process", but shouldn't
             [{:spawn, [line: 9, column: 3]}] => [{[line: 11, column: 14], [line: 2, column: 3]}]
           } == result
  end

  test "send twice to same process" do
    file = "examples/send_twice.ex"

    {:ok, extracted_ast} = ast(file)
    sends = get_sends(extracted_ast)

    result = associate(file, sends)

    assert %{
             [:main_process] => [
               {[line: 10, column: 1], [line: 4, column: 3]},
               {[line: 11, column: 1], [line: 4, column: 3]}
             ]
           } == result
  end

  test "receive in fn block" do
    file = "examples/receive_in_block.ex"
    {:ok, extracted_ast} = ast(file)
    sends = get_sends(extracted_ast)

    result = associate(file, sends)

    assert %{
             [:main_process] => [
               {[line: 10, column: 1], [line: 2, column: 3]}
             ],
             [spawn: [line: 2, column: 3]] => [{[line: 6, column: 12], [line: 10, column: 12]}]
           } == result
  end

  @tag :skip
  test "nested receive" do
    result = solve("examples/nested.ex", [line: 6, column: 12], :fns)

    assert [2] = result
  end

  @tag :skip
  test "spawn inside spawn" do
    result = solve("examples/spawn_in_spawn.ex", [line: 3, column: 10], :fns)

    assert [2] = result
  end

  @tag :skip
  test "pattern match msg" do
    file = "examples/pattern_match_receive.ex"
    {:ok, extracted_ast} = ast(file)
    sends = get_sends(extracted_ast)

    result = associate(file, sends)

    assert %{
             [:main_process] => [
               {[line: 8, column: 1], [line: 2, column: 3]}
             ]
           } == result
  end
end
